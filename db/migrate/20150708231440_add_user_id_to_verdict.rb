class AddUserIdToVerdict < ActiveRecord::Migration
  def change
    add_column :verdicts, :user_id, :integer
  end
end
