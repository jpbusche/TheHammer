class AddMovieIdToVerdict < ActiveRecord::Migration
  def change
    add_column :verdicts, :movie_id, :integer
  end
end
