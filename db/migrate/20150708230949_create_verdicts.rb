class CreateVerdicts < ActiveRecord::Migration
  def change
    create_table :verdicts do |t|
      t.integer :rating
      t.text :comment

      t.timestamps null: false
    end
  end
end
