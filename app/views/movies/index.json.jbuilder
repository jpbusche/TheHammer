json.array!(@movies) do |movie|
  json.extract! movie, :id, :name, :year, :director, :cast, :synopsis
  json.url movie_url(movie, format: :json)
end
